package Algoritmos;

public class AlgoritmosString
{
    public String invertString(String name)
    {
	String invert = "";

	for (int i = 0; i < name.length(); i++)
	{
	    invert = name.charAt(i) + invert;
	}
	return invert;
    }

    public String invertStringV2(String phrase)
    {
	StringBuffer n = new StringBuffer(phrase);

	return n.reverse().toString();
    }

    public String invertPhrase(String name)
    {
	String v[] = name.trim().split(" ");
	String invert = "";
	int size = v.length - 1;

	for (int i = size; i >= 0; i--)
	{
	    invert = invert + v[i] + " ";
	}
	return invert;
    }

    public String insertLastName(String name, String last)
    {
	StringBuffer n = new StringBuffer(name);
	int space = 0;

	for (int i = 0; i < name.length(); i++)
	{
	    if (name.charAt(i) == ' ')
	    {
		space = i;
	    }
	}
	String b = " " + last;

	return n.insert(space, b).toString();
    }

    public String citationName(String name)
    {
	String v[] = name.trim().split(" ");
	int size = v.length - 1;

	String ret = v[size];

	for (int i = 0; i < size; i++)
	{
	    ret = ret + " " + v[i].charAt(0) + ". ";
	}
	return ret;
    }

    public String initialLetters(String name)
    {
	String v[] = name.trim().split(" ");
	int size = v.length - 1;
	String ret = "";

	for (int i = 0; i < size; i++)
	{
	    ret = ret + v[i].charAt(0) + ". ";
	}
	ret = ret.toUpperCase();

	return ret;
    }

    public boolean itsFullName(String name)
    {
	String v[] = name.trim().split(" ");

	int size = v.length - 1;

	if (size > 1)
	{
	    return true;
	}
	else
	{
	    return false;
	}
    }

}