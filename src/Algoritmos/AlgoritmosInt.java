package Algoritmos;

public class AlgoritmosInt
{
    public int convertToBin(int decimal)
    {
	String v = "";
//	String x = "";
	int value = 0;

	while (decimal > 0)
	{
	    value = decimal % 2;

	    if (value == 0)
	    {
		v = v + "0";
	    }
	    else
	    {
		v = v + value;
	    }
	    decimal = decimal / 2;
	}
	StringBuffer a = new StringBuffer(v);
	a.reverse();
	
//	for (int i = 0; i < v.length(); i++)
//	{
//	    x = v.charAt(i) + x;	    
//	}	
	decimal = Integer.parseInt(v);
	return decimal;
    }
    
    public String convertDec(int i)
    {
	//converting integer to double
	String s = Integer.toString(i); //first to String	
	Double d = Double.parseDouble(s); //after to Double
	
	//perform the calculation	
	d = Math.pow(2, d);
		
	//converting double to integer
	s = Double.toString(d);
//	i = Integer.parseInt(s); 
		
	return s;
    }
}
